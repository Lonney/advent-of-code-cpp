/**
 * Project:     Advent of Code C++
 * File:        main.cpp
 * Description: Entry point of the projects.
 * Author(s):   Simon Cotts
 * Created:     09/05/2022
 */

#include "main.h"

int main()
{
    try
    {
        map<string, map<string, vector<string>>> inputs = get_inputs();
        cout << "Advent of Code C++ V0.0.1" << endl;
        cout << log_divider() << endl;
        cout << "2015:" << endl;
        cout << "  Day 1:" << endl;
        cout << "    Part 1: floor " << day_1_part_1(inputs["2015"]["day_1"][0]) << endl;
        cout << "    Part 2: instruction " << day_1_part_2(inputs["2015"]["day_1"][0]) << endl;
        cout << "  Day 2:" << endl;
        cout << "    Part 1: " << day_2_part_1(inputs["2015"]["day_2"]) << " sqft" << endl;
        cout << "    Part 2: " << day_2_part_2(inputs["2015"]["day_2"]) << " ft" << endl;
        cout << "  Day 3:" << endl;
        cout << "    Part 1: " << day_3_part_1(inputs["2015"]["day_3"][0]) << " houses" << endl;
        cout << "    Part 2: " << day_3_part_2(inputs["2015"]["day_3"][0]) << " houses" << endl;       

        cout << "PROGRAM END" << endl;
    }
    catch(const exception &exc)
    {
        cout << exc.what() << endl;
    }

    return 0;
}
