/**
 * Project:     Advent of Code C++
 * File:        inputs.h
 * Description: Inputs header.
 * Author(s):   Simon Cotts
 * Created:     10/05/2022
 */

#pragma once

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <map>
#include <string>
#include <vector>

#include "utilities.h"

using namespace std;

map<string, vector<string>> get_inputs_for_year(const string& year);
map<string, map<string, vector<string>>> get_inputs();
