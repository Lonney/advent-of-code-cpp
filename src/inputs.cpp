/**
 * Project:     Advent of Code C++
 * File:        inputs.cpp
 * Description: Reads in inputs for every challenge.
 * Author(s):   Simon Cotts
 * Created:     10/05/2022
 */

#include "inputs.h"

/**
 * Collects and returns all inputs for a given year, e.g.
 *
 * "day_1": <day_1_input>
 * "day_2": <day_2_input>
 * ...
 *
 * @year The year of inputs to target.
 * @return A map of all inputs for each day for a given year.
 */
map<string, vector<string>> get_inputs_for_year(const string& year)
{
    filesystem::path source_path = __FILE__; // E.g. /.../advent_of_code_cpp/src/inputs.cpp
    source_path = source_path.parent_path(); // E.g. /.../advent_of_code_cpp/src/
    source_path = source_path.parent_path(); // E.g. /.../advent_of_code_cpp/

    // E.g. "/.../advent_of_code_cpp/inputs/2015/"
    filesystem::path input_year_path = source_path / "inputs" / (year + '/');
    vector<string> file_paths = read_file_names_in_path(input_year_path);

    map<string, vector<string>> year_inputs;
    for(const string &file_path : file_paths)
    {
        // "/.../advent_of_code_cpp/inputs/2015/day_1.txt" -> "day_1"
        filesystem::path file_name = file_path;
        file_name = file_name.stem();

        vector<string> data = read_lines_from_file(file_path);
        year_inputs[file_name.string()] = data;
    }

    return year_inputs;
}

/*
 * Collects all the challenge inputs and returns them as a map, e.g.
 *
 * "2015":
 *   "day_1": <day_1_input>
 *   "day_2": <day_2_input>
 * "2016":
 *   "day_1": <day_1_input>
 *   "day_2": <day_2_input>
 * ...
 *
 * @return A map of the challenge inputs.
 */
map<string, map<string, vector<string>>> get_inputs()
{
    vector<string> years = {"2015"};
    map<string, map<string, vector<string>>> inputs;
    for(const string& year: years)
    {
        inputs[year] = get_inputs_for_year(year);
    }

    return inputs;
}
