/**
 * Project:     Advent of Code C++
 * File:        utilities.cpp
 * Description: General utilities.
 * Author(s):   Simon Cotts
 * Created:     10/05/2022
 */

#include "utilities.h"

/**
 * Create a string divider of a certain length.
 *
 * E.g. log_divider(10)      --> "----------"
 * E.g. log_divider(10, "*") --> "**********"
 *
 * @param length The length the divider line should be.
 * @param character The character to use for the divider.
 * @returns The formatter string divider.
 */
string log_divider(const uint8_t& length, const char& character)
{
    string divider;
    for(int i = 0; i < length; i++)
        divider += character;

    return divider;
}

//@{
/**
 * Attempts to split a string into a string vector of substrings by the specified delimiter
 * string. If no delimiter can be found, the whole string is returned in a vector.
 *
 *  E.g. the string "foo+bar+fizz+buzz": with the delimiter "+" will return
 *  {"foo", "bar", "fizz", "buzz"}
 *
 * @param str The string to split.
 * @param delimiter The delimiter to split the string by.
 * @return A string vector of the split string.
 */
vector<string> split_string(string str, const string& delimiter)
{
    size_t position = 0;
    string token;
    vector<string> split_str;

    // Find the start position of each occurrence of the delimiter, if any.
    while((position = str.find(delimiter)) != string::npos)
    {
        // Store the current substring up to where the next delimiter is.
        token = str.substr(0, position);

        // Don't save any empty strings.
        if(!token.empty())
            split_str.push_back(token);

        // Remove the current substring and delimiter.
        str.erase(0, position + delimiter.length());
    }

    // Add whatever's left after the last delimiter, if anything.
    // If no delimiter was found in the string, return the unmodified string in a vector.
    if(!str.empty())
        split_str.push_back(str);

    return split_str;
}

/**
 * Attempts to split a string into a string vector of substrings by the specified delimiter
 * character. If no delimiter can be found, the whole string is returned in a vector.
 *
 *  E.g. the string "foo+bar+fizz+buzz": with the delimiter '+' will return
 *  {"foo", "bar", "fizz", "buzz"}
 *
 * @overload
 */
vector<string> split_string(string str, const char& delimiter)
{
    size_t position = 0;
    string token;
    vector<string> split_str;

    // Find the start position of each occurence of the the delimiter.
    while((position = str.find(delimiter)) != string::npos)
    {
        // Store the current substring up to where the next delimiter is.
        // Don't save any blank or empty strings.
        token = str.substr(0, position);
        token = strip_substring(token, " ");
        if(!token.empty())
            split_str.push_back(token);
        // Remove the substring and the next delimiter.
        str.erase(0, position + 1);
    }

    // Add whatever's left after the last delimiter, if anything.
    // If no delimiter was found in the string, return the unmodified string in a vector.
    if(!str.empty())
        split_str.push_back(str);

    return split_str;
}
//@}

//@{
/**
 * Strips all instances of a substring from the provided string.
 *
 * @param str The string to modify and return.
 * @param substr The substring to be removed.
 * @return The modified string.
 */
string strip_substring(string str, const string& substr)
{
    size_t position = 0;
    while((position = str.find(substr)) != string::npos)
        str.erase(position, substr.size());

    return str;
}

/**
 * Strips all instances of a substring from each string in the provided string vector.
 *
 * @overload
 */
vector<string> strip_substring(vector<string> v_str, const string& substr)
{
    for_each(v_str.begin(), v_str.end(), [&substr](string& str)
    {
        strip_substring(str, substr);
    });

    return v_str;
}
//@}

/**
 * Reads and returns the lines from any file.
 *
 * @param file_path The path to the file to read from.
 * @returns A string vector of all lines in the file.
 */
vector<string> read_lines_from_file(const string& file_path)
{
    string line;
    vector<string> lines;
    ifstream file(file_path);

    while(getline(file, line))
        lines.push_back(line);

    file.close();
    return lines;
}

/**
 * Reads and returns all the file names in a given directory path.
 * 
 * Converts the path separators to the one preferred by the OS.
 * E.g. '\\' on _WIN32, '/' otherwise.
 *
 * @param path The target directory path.
 * @return A string vector of all file names in the target directory, if any.
 */
vector<string> read_file_names_in_path(filesystem::path path)
{
    vector<string> filenames;
    path.make_preferred(); // Convert to the OS' preferred path separator.

    for(const auto& entry: filesystem::recursive_directory_iterator(path))
        filenames.push_back(entry.path().string());

    return filenames;
}
