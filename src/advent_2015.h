/**
 * Project:     Advent of Code C++
 * File:        solutions_2015.h
 * Description: 2015 solutions header.
 * Author(s):   Simon Cotts
 * Created:     09/05/2022
 */

#pragma once

#include <algorithm>
#include <cstdint>
#include <map>
#include <set>
#include <string>
#include <tuple>

#include "utilities.h"

using namespace std;

int64_t day_1_part_1(const string& input);
uint32_t day_1_part_2(const string& input);
uint32_t day_2_part_1(const vector<string>& input);
uint32_t day_2_part_2(const vector<string>& input);
uint16_t day_3_part_1(string input);
uint16_t day_3_part_2(string input);
