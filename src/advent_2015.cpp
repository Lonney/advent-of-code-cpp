/**
 * Project:     Advent of Code C++
 * File:        advent_2015.cpp
 * Description: Santa was hoping for a white Christmas, but his weather
 * machine's "snow" function is powered by stars, and he's fresh out! To save
 * Christmas, he needs you to collect fifty stars by December 25th.
 *
 *              Collect stars by helping Santa solve puzzles. Two puzzles will
 * be made available on each day in the Advent calendar; the second puzzle is
 * unlocked when you complete the first. Each puzzle grants one star. Good luck!
 * Author(s):   Simon Cotts
 * Created:     09/05/2022
 */

#include "advent_2015.h"

// --- Day 1: Not Quite Lisp --- //

/**
 * Here's an easy puzzle to warm you up.
 * Santa is trying to deliver presents in a large apartment building, but he can't find the right
 * floor - the directions he got are a little confusing. He starts on the ground floor (floor 0) and
 * then follows the instructions one character at a time.
 *
 * An opening parenthesis, (, means he should go up one floor, and a closing parenthesis, ), means
 * he should go down one floor.
 *
 * The apartment building is very tall, and the basement is very deep; he will never find the top or
 * bottom floors.
 *
 * For example:
 *  (()) and ()() both result in floor 0.
 *  ((( and (()(()( both result in floor 3.
 *  ))((((( also results in floor 3.
 *  ()) and ))( both result in floor -1 (the first basement level).
 *  ))) and )())()) both result in floor -3.
 *
 * To what floor do the instructions take Santa?
 *
 * @param input The sequence of braces to calculate which floor santa should go to.
 * @return The floor the instructions take santa to.
 */
int64_t day_1_part_1(const string& input)
{
    // Count the number of '(' (floors up) and ')' (floors down).
    int64_t floors_up = count(input.begin(), input.end(), '(');
    int64_t floors_down = count(input.begin(), input.end(), ')');

    return floors_up - floors_down;
}


/**
 * Now, given the same instructions, find the position of the first character that causes him to
 * enter the basement (floor -1). The first character in the instructions has position 1, the second
 * character has position 2, and so on.
 *
 * For example:
 *  ) causes him to enter the basement at character position 1.
 *  ()()) causes him to enter the basement at character position 5.
 *
 *  What is the position of the character that causes Santa to first enter the basement?
 *
 *  @param input The sequence of braces to calculate which floor santa should go to.
 *  @return The floor the instructions take Santa to.
 */
uint32_t day_1_part_2(const string& input)
{
    int32_t floor = 0;
    uint32_t position = 0;

    for(; position < size(input); position++)
    {
        if(input[position] == '(')
            floor += 1;
        if(input[position] == ')')
            floor -= 1;

        if(floor == -1)
            break;
    }

    return position + 1;
}

// --- Day 2: I Was Told There Would Be No Math --- //

/**
 * The elves are running low on wrapping paper, and so they need to submit an order for more.
 * They have a list of the dimensions (length l, width w, and height h) of each present, and only
 * want to order exactly as much as they need.
 *
 * Fortunately, every present is a box (a perfect right rectangular prism), which makes calculating
 * the required wrapping paper for each gift a little easier: find the surface area of the box,
 * which is 2*l*w + 2*w*h + 2*h*l. The elves also need a little extra paper for each present: the
 * area of the smallest side.
 *
 * For example:
 *
 *  A present with dimensions 2x3x4 requires 2*6 + 2*12 + 2*8 = 52 square feet of wrapping paper
 *  plus 6 square feet of slack, for a total of 58 square feet.
 *  A present with dimensions 1x1x10 requires 2*1 + 2*10 + 2*10 = 42 square feet of wrapping paper
 *  plus 1 square foot of slack, for a total of 43 square feet.
 *
 *  All numbers in the elves' list are in feet. How many total square feet of wrapping paper
 *  should they order?
 *
 *  @param input A string vector of dimensions.
 *  @return The total area of wrapping paper required in square feet.
 */
uint32_t day_2_part_1(const vector<string>& input)
{
    uint32_t total_area = 0;
    vector<vector<string>> all_dimensions;

    // Split each dimension into its component parts: "1x2x3" -> {"1", "2", "3"}
    for(const basic_string<char>& raw_dimensions: input)
    {
        vector<string> dimensions_str = split_string(raw_dimensions, 'x');
        all_dimensions.push_back(dimensions_str);
    }

    // Calculate the total area needed for each present and add it to the running total.
    for(const vector<string>& dimensions: all_dimensions)
    {
        uint8_t length = stoi(dimensions[0]);
        uint8_t width = stoi(dimensions[1]);
        uint8_t height = stoi(dimensions[2]);
        uint16_t area_lxw = length * width;
        uint16_t area_wxh = width * height;
        uint16_t area_hxl = height * length;
        vector<uint16_t> side_areas{area_lxw, area_wxh, area_hxl};

        uint16_t smallest_side_area = *min_element(side_areas.begin(), side_areas.end());
        uint16_t area = 2 * (area_lxw + area_wxh + area_hxl);
        total_area += area + smallest_side_area;
    }

    return total_area;
}

/**
 * The elves are also running low on ribbon. Ribbon is all the same width, so they only have to
 * worry about the length they need to order, which they would again like to be exact.
 * The ribbon required to wrap a present is the shortest distance around its sides, or the smallest
 * perimeter of any one face. Each present also requires a bow made out of ribbon as well; the feet
 * of ribbon required for the perfect bow is equal to the cubic feet of volume of the present. Don't
 * ask how they tie the bow, though; they'll never tell.
 *
 * For example:
 *  A present with dimensions 2x3x4 requires 2+2+3+3 = 10 feet of ribbon to wrap the present plus
 *  2*3*4 = 24 feet of ribbon for the bow, for a total of 34 feet.
 *  A present with dimensions 1x1x10 requires 1+1+1+1 = 4 feet of ribbon to wrap the present plus
 *  1*1*10 = 10 feet of ribbon for the bow, for a total of 14 feet.
 *
 *  How many total feet of ribbon should they order?
 *
 *  @param input A string vector of dimensions.
 *  @return The total length of ribbon required in feet.
 */
uint32_t day_2_part_2(const vector<string>& input)
{
    uint32_t total_length = 0;
    vector<vector<string>> all_dimensions;

    // Split each dimension into its component parts: "1x2x3" -> {"1", "2", "3"}
    for(const basic_string<char>& raw_dimensions: input)
    {
        vector<string> dimensions_str = split_string(raw_dimensions, 'x');
        all_dimensions.push_back(dimensions_str);
    }

    // Calculate the total area needed for each present and add it to the running total.
    for(const vector<string>& dimensions: all_dimensions)
    {
        uint8_t length = stoi(dimensions[0]);
        uint8_t width = stoi(dimensions[1]);
        uint8_t height = stoi(dimensions[2]);
        uint16_t perimeter_lxw = 2 * (length + width);
        uint16_t perimeter_wxh = 2 * (width + height);
        uint16_t perimeter_hxl = 2 * (height + length);
        vector<uint16_t> side_perimeters{perimeter_lxw, perimeter_wxh, perimeter_hxl};

        uint16_t smallest_perimiter = *min_element(
            side_perimeters.begin(), side_perimeters.end()
        );
        uint16_t volume = length * width * height;
        total_length += smallest_perimiter + volume;
    }

    return total_length;
}

// --- Day 3: Perfectly Spherical Houses in a Vacuum --- //

/**
 * Santa is delivering presents to an infinite two-dimensional grid of houses.
 *
 * He begins by delivering a present to the house at his starting location, and then an elf at the
 * North Pole calls him via radio and tells him where to move next. Moves are always exactly one
 * house to the north (^), south (v), east (>), or west (<). After each move, he delivers another
 * present to the house at his new location.
 *
 * However, the elf back at the north pole has had a little too much eggnog, and so his  directions
 * are a little off, and Santa ends up visiting some houses more than once. How many houses receive
 * at least one present?
 *
 * For example:
 *
 *   - > delivers presents to 2 houses: one at the starting location, and one to the east.
 *   - ^>v< delivers presents to 4 houses in a square, including twice to the house at his
 *     starting/ending location.
 *   - ^v^v^v^v^v delivers a bunch of presents to some very lucky children at only 2 houses.
 *
 * @param input A string of all navigation instructions.
 * @return The number of houses that receive one or more presents.
 */
uint16_t day_3_part_1(string input)
{
    int16_t x = 0; // Index of the x coordinate.
    int16_t y = 1; // Index of the y coordinate.
    tuple<int16_t, int16_t> east = {x, 1}; // A move east increments the x coordinate.
    tuple<int16_t, int16_t> west = {x, -1}; // A move west decrements the x coordinate.
    tuple<int16_t, int16_t> north = {y, 1}; // A move north increments the y coordinate.
    tuple<int16_t, int16_t> south = {y, -1}; // A move south decrements the y coordinate.

    // Map each direction instruction to its coordinate instruction.
    map<char, tuple<int16_t, int16_t>> direction_map = {
        {'>', east},
        {'<', west},
        {'^', north},
        {'v', south}
    };

    vector<vector<int32_t>> house_coordinates = {{0, 0}};
    for(uint32_t idx = 0; idx < input.length(); idx++)
    {
        // Start off from the last coordinates.
        house_coordinates.push_back(house_coordinates[idx]);
        char direction = input[idx];
        int32_t coordinate = 0;
        int32_t modifier = 0;
        tie(coordinate, modifier) = direction_map[direction];

        house_coordinates[idx + 1][coordinate] += modifier;
    }

    set<vector<int32_t>> unique_coordinates;
    for(const vector<int32_t>& coord: house_coordinates)
        unique_coordinates.insert(coord);

    return unique_coordinates.size();
}

/**
 * The next year, to speed up the process, Santa creates a robot version of himself, Robo-Santa, to
 * deliver presents with him.
 *
 * Santa and Robo-Santa start at the same location (delivering two presents to the same starting
 * house), then take turns moving based on instructions from the elf, who is eggnoggedly reading
 * from the same script as the previous year.
 *
 * This year, how many houses receive at least one present?
 *
 * For example:
 *
 *  - ^v delivers presents to 3 houses, because Santa goes north, and then Robo-Santa goes south.
 *  - ^>v< now delivers presents to 3 houses, and Santa and Robo-Santa end up back where they
 *    started.
 *  - ^v^v^v^v^v now delivers presents to 11 houses, with Santa going one direction and Robo-Santa
 *    going the other.
 *
 * @param input A string of all navigation instructions.
 * @return The number of houses that receive one or more presents.
 */
uint16_t day_3_part_2(string input)
{
    int16_t x = 0; // Index of the x coordinate.
    int16_t y = 1; // Index of the y coordinate.
    tuple<int16_t, int16_t> east = {x, 1}; // A move east increments the x coordinate.
    tuple<int16_t, int16_t> west = {x, -1}; // A move west decrements the x coordinate.
    tuple<int16_t, int16_t> north = {y, 1}; // A move north increments the y coordinate.
    tuple<int16_t, int16_t> south = {y, -1}; // A move south decrements the y coordinate.

    // Map each direction instruction to its coordinate instruction.
    map<char, tuple<int16_t, int16_t>> direction_map = {
        {'>', east},
        {'<', west},
        {'^', north},
        {'v', south}
    };

    vector<char> santa_directions;
    vector<char> robot_directions;
    for(uint32_t idx = 0; idx < input.length(); idx += 2)
    {
        santa_directions.push_back(input[idx]);
        robot_directions.push_back(input[idx + 1]);
    }

    vector<vector<int32_t>> all_house_coordinates;
    for(const vector<char>& directions: {santa_directions, robot_directions})
    {
        vector<vector<int32_t>> house_coordinates = {{0, 0}};
        for(uint32_t idx = 0; idx < directions.size(); idx++)
        {
            // Start off from the last coordinates.
            house_coordinates.push_back(house_coordinates[idx]);
            char direction = directions[idx];
            int32_t coordinate = 0;
            int32_t modifier = 0;
            tie(coordinate, modifier) = direction_map[direction];

            house_coordinates[idx + 1][coordinate] += modifier;
        }

        concatenate(all_house_coordinates, house_coordinates, all_house_coordinates);
    }

    set<vector<int32_t>> unique_coordinates;
    for(const vector<int32_t>& coord: all_house_coordinates)
        unique_coordinates.insert(coord);

    return unique_coordinates.size();
}
