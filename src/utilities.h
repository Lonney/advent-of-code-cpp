/**
 * Project:     Advent of Code C++
 * File:        utilities.cpp
 * Description: General utilities header.
 * Author(s):   Simon Cotts
 * Created:     10/05/2022
 */

#pragma once

#include <array>
#include <algorithm>
#include <cstdint>
#include <filesystem>
#include <fstream>
#include <string>
#include <vector>
#include <iostream>

using namespace std;

string log_divider(const uint8_t& length = 25, const char& character = '-');

vector<string> split_string(string str, const string& delimiter);
vector<string> split_string(string str, const char& delimiter);

string strip_substring(string str, const string& substr);
vector<string> strip_substring(vector<string> v_str, const string& substr);

vector<string> read_lines_from_file(const string &file_path);
vector<string> read_file_names_in_path(filesystem::path path);

template<typename T>
void concatenate(vector<T>& a, vector<T>& b, vector<T>& ab);

/**
 * Concatenates two vectors together.
 *
 * @tparam T The type stored by the vector.
 * @param a The first vector.
 * @param b The second vector.
 * @param ab The vector that will store the contents of the first and second vectors.
 */
template<typename T>
void concatenate(vector<T>& a, vector<T>& b, vector<T>& ab)
{
    ab.reserve(a.size() + b.size());
    ab.insert(ab.end(), a.begin(), a.end());
    ab.insert(ab.end(), b.begin(), b.end());
}
