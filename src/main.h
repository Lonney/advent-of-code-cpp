/**
 * Project:     Advent of Code C++
 * File:        main.h
 * Description: Entry point header.
 * Author(s):   Simon Cotts
 * Created:     17/05/2022
 */

#pragma once

#include <exception>
#include <map>
#include <iostream>
#include <vector>

#include "advent_2015.h"
#include "inputs.h"
#include "utilities.h"

using namespace std;
